package QuicksortMusicPlayer;

public class Tunes {
    public static void main(String[] args) {
        Album music = new Album();

        music.addTitle("By the Way", "Red Hot Chili Peppers", 3);
        music.addTitle("Come On Over", "Shania Twain", 5);
        music.addTitle("Soundtrack", "The Producers", 10);
        music.addTitle("Play", "Jennifer Lopez", 1);


        music.addTitle("Double Live", "Garth Brooks", 2);
        music.addTitle("Greatest Hits", "Stone Temple Pilots", 4);

        System.out.println(music);

        var begin = 0;
        var end = 5;

        music.setList(Album.QuickSort(music.getList(), begin, end));
//        music.setList(Album.InsertionSortByTitle(music.getList(), false));
        System.out.println(music);
    }
}
