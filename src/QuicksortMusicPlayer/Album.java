package QuicksortMusicPlayer;

import java.util.ArrayList;

public class Album {
    private ArrayList<Title> list = new ArrayList<Title>();

    public void addTitle(String title, String singer, int price) {
        Title item = new Title(title, singer, price);
        list.add(item);
    }

    public ArrayList<Title> getList() {
        return this.list;
    }

    public void setList(ArrayList<Title> arrayList) {
        this.list = arrayList;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Title title : list) {
            result.append(", ").append(((Title) title).getTitle());
        }

        return result.toString();
    }


    public static ArrayList<Title> QuickSort(ArrayList<Title> arr, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(arr, begin, end);

            if (partitionIndex > 1) {
                QuickSort(arr, begin, partitionIndex-1);
            }
            if (partitionIndex + 1 < end) {
                QuickSort(arr, partitionIndex+1, end);
            }
        }
        return arr;
    }


    private static int partition(ArrayList<Title> arr, int begin, int end) {
        Title pivot = arr.get(end);
        int i = (begin-1);

        for (int j = begin; j < end; j++) {
            if (arr.get(j).getSongLength() <= pivot.getSongLength()) {
                i++;
                Title swapTemp = arr.get(i);
                arr.set(i, arr.get(j));
                arr.set(j, swapTemp);
            }
        }

        Title swapTemp = arr.get(i + 1);
        arr.set(i + 1, arr.get(end));
        arr.set(end, swapTemp);

        return i+1;
    }

}
